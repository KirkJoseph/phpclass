<?php

/*
 *  This is a countdown timer
 *  End of Semester: 7-31-2020
 */

    $secPerMinute = 60;
    $secPerHour = 60 * $secPerMinute;
    $secPerDay = 24 * $secPerHour;
    $secPerYear = 365 * $secPerDay;

    // Current Time
    $now = time();

    //Semester end time
    $semesterEnd = mktime(12,0,0,7,31,2020);

    //Number of seconds between now and then
    $seconds = $semesterEnd - $now;

    $Years = floor($seconds / $secPerYear);
    $seconds = $seconds - ($Years * $secPerYear);

    $Days = floor($seconds / $secPerDay);
    $seconds = $seconds - ($Days * $secPerDay);

    $Hours = floor($seconds / $secPerHour);
    $seconds = $seconds - ($Hours * $secPerHour);

    $Minutes = floor($seconds / $secPerMinute);
    $seconds = $seconds - ($Minutes * $secPerMinute);



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>

    <h3>End of Semester Countdown</h3>
    <p>Days: <?=$Days ?> | Hours: <?=$Hours ?> | Minutes: <?=$Minutes ?> | Seconds: <?=$seconds ?></p>

</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>

