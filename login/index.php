<?php
session_start();

// see if variables are set due to form submission
if(isset($_POST["txtEmail"])){
    if(isset($_POST["txtPassword"])){

            $userName = $_POST["txtEmail"];
            $pwd = $_POST["txtPassword"];
            $errmsg = "";

        include '../includes/dbConn.php';

        // attempt data base connection w/exception handling
        try {
            $db = new PDO($dsn, $username, $password, $options);

            $sql = $db->prepare('select memberID, memberPassword, memberKey, RoleID from memberLogin where memberEmail = :Email');

            $sql->bindValue(":Email", $userName);

            $sql->execute();

            $row = $sql->fetch();

            if($row!=null) {

                $hashedPassword = md5($pwd . $row["memberKey"]);

                if ($hashedPassword == $row["memberPassword"]) {

                    $_SESSION["UID"] = $row["memberID"];
                    $_SESSION["Role"] = $row["RoleID"];

                    if ($_SESSION["Role"] == 1) {

                        header("Location:admin.php");

                    } else {

                        header("Location:member.php");

                    }
                } else {

                    $errmsg = "Wrong Username or Password";
                }

            } else {

                $errmsg = "Wrong Username or Password";

            }

            // display error message if there was an exception
        }catch (PDOException $e){

            $error = $e->getMessage();
            echo "Error: $error";

        }

    }

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <form method="post">
        <h3 id="error"><?=$errmsg?></h3>
        <table border="1" width="80%">
            <tr height="60px">
                <th colspan="2"><h3>User Login</h3></th>
            </tr>
            <tr height="60px">
                <th>Email</th>
                <td><input id="txtEmail" name="txtEmail" type="text" size="50" value=""></td>
            </tr>
            <tr height="60px">
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password" size="50" value=""></td>
            </tr>
            <tr height="60px">
                <td colspan="2"><input type="submit" value="Login"></td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
