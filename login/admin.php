<?php
session_start();

$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
//$errmsg = "";

if($_SESSION["Role"] != 1){

    header("Location:index.php");
}

if (isset($_POST["submit"])) {

    if(empty($_POST["txtFName"])) {

        $errmsg = "Name is required";

    } else {

        $FName = $_POST["txtFName"];
    }

    if(empty($_POST["txtEmail"])) {

        $errmsg = "Email is required";

    } else {

        $Email = $_POST["txtEmail"];
    }

    if(empty($_POST["txtPassword"])) {

        $errmsg = "Password is required";

    } else {

        $Password = $_POST["txtPassword"];

    }

    if ($Password != $_POST["txtPassword2"]) {

        $errmsg = "Passwords do not match";

    }

    if(empty($_POST["txtRole"])) {

        $errmsg = "Role is required";

    } else {

        $Role = $_POST["txtRole"];

    }

    if ($errmsg=="") {

        include '../includes/dbConn.php';

        try {
            $db = new PDO($dsn, $username, $password, $options);

            // insert form submission into table
            $sql = $db->prepare('insert into memberLogin (memberName, memberEmail, memberPassword, RoleID, memberKey) value (:Name, :Email, :Password, :RID, :Key)');

            $sql->bindValue(":Name", $FName);
            $sql->bindValue(":Email", $Email);
            $sql->bindValue(":Password", md5($Password . $key));
            $sql->bindValue(":RID", $Role);
            $sql->bindValue(":Key", $key);

            $sql->execute();

            // display error message if there was an exception
        }catch (PDOException $e){

            $error = $e->getMessage();
            echo "Error: $error";

        }

        $FName = "";
        $Email = "";
        $Password = "";
        $Role = "";

        $errmsg = "Member Added To Database";

    }
}



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <h1>Admin Page</h1>
    <form method="post">
        <h3 id="error"><?=$errmsg?></h3>
        <table border="1" width="80%">
            <tr height="60px">
                <th colspan="2"><h3>Add New Member</h3></th>
            </tr>
            <tr height="60px">
                <th>Full Name</th>
                <td><input id="txtFName" name="txtFName" type="text" size="50" value="<?=$FName?>"></td>
            </tr>
            <tr height="60px">
                <th>Email</th>
                <td><input id="txtEmail" name="txtEmail" type="text" size="50" value="<?=$Email?>"></td>
            </tr>
            <tr height="60px">
                <th>Password</th>
                <td><input id="txtPassword2" name="txtPassword2" type="password" size="50"></td>
            </tr>
            <tr height="60px">
                <th>Retype Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password" size="50"></td>
            </tr>
            <tr height="60px">
                <th>Role</th>
                <td>
                    <select id="txtRole" name="txtRole" value="<?=$Role?>">

                    <?php
                        include '../includes/dbConn.php';

                        // attempt data base connection w/exception handling
                        try {
                        $db = new PDO($dsn, $username, $password, $options);

                        // pull everything from table movielist
                        $sql = $db->prepare('select * from role');

                        $sql->execute();
                        $row = $sql->fetch();

                        // while we are getting data from table, display accordingly
                        while ($row!=null){

                            echo "<option value=".$row["RoleID"].">".$row["RoleValue"]."</option>";

                        $row = $sql->fetch();

                        }

                        // display error message if there was an exception
                        }catch (PDOException $e){

                        $error = $e->getMessage();
                        echo "Error: $error";

                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr height="60px">
                <td colspan="2"><input type="submit" value="Add New Member" name="submit"></td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
