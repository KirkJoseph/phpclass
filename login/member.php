<?php
session_start();

// if not admin direct to index page
if(!isset($_SESSION["UID"])){

    header("Location:index.php");
}

$roleID = $_SESSION["Role"];

switch ($roleID) {

    case 1:
        $roletype = "Admin";
        break;
    case 2:
        $roletype = "Operator";
        break;
    case 3:
        $roletype = "Member";
        break;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <h1>Member Page</h1>
    <br /><br />
    <h3>Welcome, <?=$roletype?></h3>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
