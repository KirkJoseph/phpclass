<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
	    // load form helper and display public view
	    $this->load->helper('form');
		$this->load->view('public/home');
	}

    public function login()
    {

        // validate login form fields
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        // if validation fails generate error and return to public home view
        if ($this->form_validation->run() == false) {

            $data = array('load_error' => 'true');

            $this->load->view('public/home', $data);

        } else {

            // load Member model
            $this->load->model('Member');

            // capture form data
            $usn = $this->input->post('user_name');
            $psw = $this->input->post('password');

            // pass form data to model for login function
            if ($this->Member->user_login($usn, $psw)) {

                // pass to admin home view if login successful
                $this->load->view('admin/dashboard');

            } else {

                // if not successful load error message and pass back to public home view
                $data = array('load_error' => 'true', 'error_message' => 'Invalid Username or Password');
                $this->load->view('public/home', $data);

            }

        }

    }

    public function create()
    {

        //validate create account form fields
        $this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'E-Mail Address', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('password_rep', 'Repeat Password', 'trim|required');

        if ($this->form_validation->run() == false) {

            // if validation fails generate error and return to public home view
            $data = array('load_error' => 'true');

            $this->load->view('public/home', $data);

        } else {

            // validation passes, load Member model
            $this->load->model('Member');

            // capture form data
            $fname = $this->input->post('full_name');
            $email = $this->input->post('email');
            $psw = $this->input->post('password');
            $psw_rep = $this->input->post('password_rep');

            // pass form data to create_user function
            if ($this->Member->create_user($fname, $email, $psw, $psw_rep)) {

                // if create_user function successful, display message and return to public home view for login
                $data = array('load_error' => 'true', 'error_message' => 'User created successfully.');
                $this->load->view('public/home', $data);

            } else {

                // if create_user function fails, display error message and return to public home view for login
                $data = array('load_error' => 'true', 'error_message' => 'Unable to create user, e-mail address already registered.');
                $this->load->view('public/home', $data);

            }

        }

    }
}
