<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Race extends CI_Model
{
    public function get_races()
    {

        $this->load->database();

        try {

            $query = $this->db->get('race');

            return $query->result_array();

        } catch (PDOException $e) {

            $error = $e->getMessage();
            echo "Error: $error";

        }

    }

    public function get_race($id)
    {

        $this->load->database();

        try {

            $data = array('race_id'=>$id);
            $query = $this->db->get_where('race', $data);

            return $query->result_array();

        } catch (PDOException $e) {

            $error = $e->getMessage();
            echo "Error: $error";

        }

    }


    public function add_race($name, $location, $description, $timestamp)
    {

        $this->load->database();

        try {

            $data=array('race_name'=>$name, 'race_location'=>$location, 'race_desc'=>$description, 'race_timestamp'=>$timestamp);
            $query = $this->db->insert('race', $data);

        } catch (PDOException $e) {

            $error = $e->getMessage();
            echo "Error: $error";

        }

    }

    public function delete_race($id){

        $this->load->database();

        try {

            $data=array('race_id'=>$id);
            $query = $this->db->delete('race', $data);

            //return $query->result_array();

        } catch (PDOException $e) {

            $error = $e->getMessage();
            echo "Error: $error";

        }

    }

    public function update_race($name, $location, $description, $timestamp, $id)
    {

        $this->load->database();

        try {

            $data=array('race_name'=>$name, 'race_location'=>$location, 'race_desc'=>$description, 'race_timestamp'=>$timestamp);

            $this->db->where('race_id', $id);

            $query = $this->db->update('race', $data);

        } catch (PDOException $e) {

            $error = $e->getMessage();
            echo "Error: $error";

        }

    }

}