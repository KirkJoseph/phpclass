<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model
{

    public function user_login($email, $pwd)
    {

        // initiate database
	    $this->load->database();

	    // initiate session
	    $this->load->library('session');

        try {

            // attempt to find the user based on email address and role_id
            $db = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);

            $sql = $db->prepare('select memberID, memberPassword, memberKey from memberLogin where memberEmail = :Email and RoleID = 2');

            $sql->bindValue(":Email", $email);

            $sql->execute();

            $row = $sql->fetch();

            // if user found, verify stored password matches input
            if($row!=null) {

                $hashedPassword = md5($pwd . $row["memberKey"]);

                if ($hashedPassword == $row["memberPassword"]) {

                    $this->session->set_userdata(array('UID'=>$row["memberID"]));
                    //$_SESSION["UID"] = $row["memberID"];
                    //$_SESSION["Role"] = $row["RoleID"];

                    return true;


                } else {

                    return false;
                }

            } else {

                return false;

            }

            // display error message if there was an exception
        }catch (PDOException $e){

            $error = $e->getMessage();
            echo "Error: $error";

        }

	}

	public function create_user($fullname, $email, $pwd, $pwd_rep){

        // generate key
        $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

        // initiate database connection
        $this->load->database();

        // initiate session
        $this->load->library('session');

        // verify password inputs match, otherwise generate error message and fail
        if ($pwd!=$pwd_rep){

            $data = array('load_error' => 'true', 'error_message' => 'Passwords do not match');
            $this->load->view('public/home', $data);

            return false;

        } else {
            // if passwords do match, attempt to write new user to database
            try {

                $db = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);

                $sql = $db->prepare('insert into memberLogin (memberName, memberEmail, memberPassword, RoleID, memberKey) value (:Name, :Email, :Password, :RID, :Key)');

                $sql->bindValue(":Name", $fullname);
                $sql->bindValue(":Email", $email);
                $sql->bindValue(":Password", md5($pwd . $key));
                $sql->bindValue(":RID", 2);
                $sql->bindValue(":Key", $key);

                $sql->execute();

                return true;

            } catch (PDOException $e) {

                $error = $e->getMessage();
                echo "Error: $error";

            }

        }


    }

}
