<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RunningMan - Registration Form</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=asset_url()?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=asset_url()?>css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=asset_url()?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php

            $this->load->view('includes/header');
            $this->load->view('includes/menu');

            ?>
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <div class="page-header">
                    <h1>Registration Form</h1>
                </div>
                <div class="well">
                    <p>
                        PARTICIPANTS IN THE 2020 Marathon and the KID’S MARATHON (collectively, the “Marathon” ) ARE REQUIRED TO
                        ASSUME ALL RISK OF PARTICIPATION IN THE MARATHON AND TRAINING BY SIGNING THIS GENERAL RELEASE
                        AGREEMENT. The undersigned athlete (“Athlete”), or if the Athlete is below the age of 18, the parent or legal guardian of the Athlete on
                        behalf of the Athlete (the “Parent”), on behalf of himself/herself and on behalf of Athlete’s personal representatives, assigns, heirs, executors,
                        hereby fully and forever releases, waives, discharges and covenants not to sue me, the State of Wisconsin,
                        Calumet, the Village of Harrison, Theda Medical Center, Kaukauna Chamber of Commerce, Menasha Joint School District, YMCA, USATF
                        and all municipal agencies and school districts whose property and/or personnel are used, and all other sponsoring or cosponsoring companies or
                        individuals related to the Marathon, and the officers, directors and employees of each of the foregoing and all individuals volunteering to
                        support the Marathon (collectively “Releasees”) from all liability to the Athlete and his/her personal representatives, assigns, heirs and
                        executors, for all loss(es) or damage(s) and any and all claims or demands therefore, on account of injury to the Athlete or property or resulting
                        in the death of the Athlete, whether caused by the active or passive negligence of all or any of the Releases or otherwise, in connection with the
                        Athlete’s participation in the Marathon event and during training for event. The Athlete (or Parent on behalf of the Athlete) represents and
                        warrants that he/she is in good physical condition and is able to safely participate in the Marathon and training activities. The Athlete (or Parent
                        on behalf of the Athlete) is fully aware of the risks and hazards inherent in participating in the Marathon and training program and hereby elects
                        to voluntarily compete in the Marathon and training program, knowing the risks associated with the Marathon and training program. The Athlete
                        (or Parent on behalf of the Athlete) hereby assumes all risks of loss(es), damage(s), or injury(ies) that may be sustained by him/her while
                        participating in the Marathon and/or training program. The Athlete (or Parent on behalf of the Athlete) agrees to the use of his/her name and
                        photograph in broadcasts, newspapers, brochures and other media without compensation. The Athlete (or Parent on behalf of the Athlete)
                        acknowledges that the entry fee is nontransferable. In the event the Marathon is delayed or prevented by reason of fire, threatened or actual
                        strike, labor difficulty, work stoppage, insurrection war, public disaster, flood, unavoidable casualty, acts of God or the elements (included
                        without limitation, ice storms, blizzards, tornadoes), or any other cause beyond the control of the Event Race Director, there may not be a make
                        up or rescheduling of the events. The Athlete (or Parent on behalf of the Athlete) hereby grants to the medical doctor(s) of the Marathon, and his
                        agents, affiliates and designees, access to all medical records (and physicians) as needed and authorizes medical treatment as needed. The
                        Athlete (or Parent on behalf of the Athlete) warrants that all statements made herein are true and correct and understands that Releasees have
                        relied on them in allowing Athlete to participate in the Marathon. ATHLETE (OR PARENT ON BEHALF OF THE ATHLETE) HAS READ
                        THE FOREGOING AND INTENTIONALLY AND VOLUNTARILY SIGNS THIS RELEASE AND WAIVER OF LIABILITY
                        AGREEMENT.<br /><br />
                        Athlete (or Parent if under 18) Signature:______________________________________ Date:__________________________<br /><br />
                        Athlete (or Parent if under 18) Name (please print)_________________ _____________________________________________
                    </p>
                </div>
                <button type="" class="btn btn-default">Print Form</button>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=asset_url()?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=asset_url()?>js/bootstrap.min.js"></script>

</body>

</html>
