<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RunningMan - Manage Runners</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=asset_url()?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=asset_url()?>css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=asset_url()?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php

                $this->load->view('includes/header');
                $this->load->view('includes/menu');

            ?>
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Runners
                        </h1>
                    </div>
                </div>
                <!-- /.row -->



                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Race:</label>
                            <select class="form-control">
                                <option>1. Joe's 5k</option>
                                <option>2. Bob's 10k</option>
                                <option>3. Fox Cities Marathon</option>
                                <option>4. Bellin Fun Run</option>
                                <option>5. Festival Turkey Trot</option>
                            </select>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Runner Name</th>
                                        <th>Currently Registered For</th>
                                        <th>Total Races Ran</th>
                                        <th>Average Finish Time</th>
                                        <th>Manage Runner</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="active">
                                        <td>Joseph Kirk</td>
                                        <td>7</td>
                                        <td>24</td>
                                        <td>01:37:04</td>
                                        <td><a href="#">Remove</a></td>
                                    </tr>
                                    <tr class="success">
                                        <td>Amber Kirk</td>
                                        <td>261</td>
                                        <td>7,821</td>
                                        <td>00:01:00</td>
                                        <td><a href="#">Remove</a></td>
                                    </tr>
                                    <tr class="warning">
                                        <td>Micah Kirk</td>
                                        <td>0</td>
                                        <td>4</td>
                                        <td>04:22:17</td>
                                        <td><a href="#">Remove</a></td>
                                    </tr>
                                    <tr class="danger">
                                        <td>Lucas Kirk</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>---</td>
                                        <td><a href="#">Remove</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=asset_url()?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=asset_url()?>js/bootstrap.min.js"></script>

</body>

</html>
