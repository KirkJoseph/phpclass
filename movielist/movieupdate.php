<?php

include '../includes/dbConn.php';

// see if variables are set due to form submission
if(isset($_POST["txtTitle"])){
    if(isset($_POST["txtRating"])){

        $title = $_POST["txtTitle"];
        $rating = $_POST["txtRating"];
        $id = $_POST["txtID"];

        try {
            $db = new PDO($dsn, $username, $password, $options);

            // insert form submission into table
            $sql = $db->prepare("update movielist set movieTitle = :Title, movieRating = :Rating where movieID = :ID");

            $sql->bindValue(":Title", $title);
            $sql->bindValue(":Rating", $rating);
            $sql->bindValue( ":ID", $id);

            $sql->execute();

            // display error message if there was an exception
        }catch (PDOException $e){

            $error = $e->getMessage();
            echo "Error: $error";

        }

        header("Location:movielist.php");

    }
}

// populate form data if id variable is set
if(isset($_GET["id"]))
{

    $id = $_GET["id"];
    try {
        $db = new PDO($dsn, $username, $password, $options);

        // select movie record based on ID passed from movielist

        $sql = $db->prepare("select * from movielist where movieID = :id");

        $sql->bindValue(":id", $id);
        //$sql->bindValue(":Rating", $rating);

        $sql->execute();
        $row = $sql->fetch();

        $rating = $row["movieRating"];
        $title = $row["movieTitle"];

        // display error message if there was an exception
    } catch (PDOException $e) {

        $error = $e->getMessage();
        echo "Error: $error";

    }

} else {

    header("Location:movielist.php");

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <script type="text/javascript">

        function delete_movie(title,movieid){

            if(confirm("Do you want to delete " + title + "?")){

                document.location.href = "moviedelete.php?id=" + movieid;

            }

        }

    </script>
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <form method="post">
        <table border="1" width="80%">
            <tr height="60px">
                <th colspan="2"><h3>Update Movie</h3></th>
            </tr>
            <tr height="60px">
                <th>Movie Name</th>
                <td><input id="txtTitle" name="txtTitle" type="text" size="50" value="<?=$title?>"></td>
            </tr>
            <tr height="60px">
                <th>Movie Rating</th>
                <td><input id="txtRating" name="txtRating" type="text" size="50" value="<?=$rating?>"></td>
            </tr>
            <tr height="60px">
                <td colspan="2"><input type="submit" value="Update Movie"> | <input type="button" onclick="delete_movie('<?=$title?>','<?=$id?>')" value="Delete Movie"></td>
            </tr>
        </table>
        <input type="hidden" id="txtID" name="txtID" value="<?=$id?>">
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
