<?php
// see if variables are set due to form submission
if(isset($_POST["txtTitle"])){
    if(isset($_POST["txtRating"])){

        $title = $_POST["txtTitle"];
        $rating = $_POST["txtRating"];

        include '../includes/dbConn.php';

        try {
            $db = new PDO($dsn, $username, $password, $options);

            // insert form submission into table
            $sql = $db->prepare('insert into movielist (movieTitle, movieRating) value (:Title, :Rating)');

            $sql->bindValue(":Title", $title);
            $sql->bindValue(":Rating", $rating);

            $sql->execute();

            // display error message if there was an exception
        }catch (PDOException $e){

            $error = $e->getMessage();
            echo "Error: $error";

        }

        header("Location:movielist.php");

    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <form method="post">
        <table border="1" width="80%">
            <tr height="60px">
                <th colspan="2"><h3>Add New Movie</h3></th>
            </tr>
            <tr height="60px">
                <th>Movie Name</th>
                <td><input id="txtTitle" name="txtTitle" type="text" size="50"></td>
            </tr>
            <tr height="60px">
                <th>Movie Rating</th>
                <td><input id="txtRating" name="txtRating" type="text" size="50"></td>
            </tr>
            <tr height="60px">
                <td colspan="2"><input type="submit" value="Add New Movie"></td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
