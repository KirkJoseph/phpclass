<?php

    // initialize player score array, fill two elements with random number between 1 and 6
    $playerScore = array();
    $playerScore[0] = mt_rand(1,6);
    $playerScore[1] = mt_rand(1,6);

    // calculate sum of array elements
    $playerTotal = array_sum($playerScore);

    // initiate comp score array, fill three elements with random number between 1 and 6
    $compScore = array();
    $compScore[0] = mt_rand(1,6);
    $compScore[1] = mt_rand(1,6);
    $compScore[2] = mt_rand(1,6);

    // calculate sum of array elements
    $compTotal = array_sum($compScore);

    // determine winner or tie
    if($playerTotal > $compTotal){

        $result = "You win!";

    }elseif($playerTotal < $compTotal) {

        $result = "Computer wins!";
    }else{

        $result = "Tie game!";

    }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <h1>Your Score: <?=$playerTotal?></h1>

    <?php

    // subtract 1 to account for array starting at 0
    for($i=0;$i<=(count($playerScore)-1);$i++){

        // display appropriate dice image based on the value of the element
        $filename = "images/dice_" .$playerScore[$i]. ".png";
        echo "<img src='$filename' class='dice'>";
    }

    ?>
    <h1>Computer's Score: <?=$compTotal?></h1>

    <?php

    // subtract 1 to account for array starting at 0
    for($i=0;$i<=(count($compScore)-1);$i++){

        // display appropriate dice image based on the value of the element
        $filename = "images/dice_" .$compScore[$i]. ".png";
        echo "<img src='$filename' class='dice'>";
    }

    // display result
    echo "<br /><br /><h1>Result: $result</h1>";

    ?>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
