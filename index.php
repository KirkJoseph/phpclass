<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css">
</head>
<body>
    <header><?php include 'includes/header.php'?></header>
    <nav><?php include 'includes/nav.php'?></nav>
    <main>
        <img src="images/joe.jpg" alt="Picture of Tanooki Mario">
        <p>Hello, my name is Joseph Kirk.  This is the website for all of my PHP class assignments.  Thanks for looking, and have a nice day!</p>
    </main>
    <footer><?php include 'includes/footer.php'?></footer>
</body>
</html>