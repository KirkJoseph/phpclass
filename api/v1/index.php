<?php
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/get_races','get_races');
$app->get('/get_runners/:RaceID', 'get_runners');
$app->post('/add_runner','add_runner');
$app->delete('/delete_runner','delete_runner');

$app->run();

function get_races(){

    include '../../includes/dbConn.php';

    // attempt data base connection w/exception handling
    try {
        $db = new PDO($dsn, $username, $password, $options);

        $sql = $db->prepare('select * from race');

        $sql->execute();

        $result = $sql->fetchAll();

        echo '{"Races": '.json_encode($result) . '}';

        $result = null;
        $db = null;

    }catch (PDOException $e){

        $error = $e->getMessage();
        echo json_encode($error);

    }

}

function get_runners($RaceID){

    include '../../includes/dbConn.php';

    // attempt data base connection w/exception handling
    try {
        $db = new PDO($dsn, $username, $password, $options);


        $sql = $db->prepare("SELECT DISTINCT memberLogin.memberName,memberLogin.memberEmail,member_race.race_id FROM memberLogin INNER JOIN	member_race	ON memberLogin.memberID = member_race.member_id WHERE member_race.race_id = :race_id");

        $sql->bindValue(":race_id", $RaceID);
        $sql->execute();

        $result = $sql->fetchAll();

        echo '{"Runners": '.json_encode($result) . '}';

        $result = null;
        $db = null;

    }catch (PDOException $e){

        $error = $e->getMessage();
        echo json_encode($error);

    }

    //echo "Runner listing for race: $RaceID";

}

function add_runner(){

    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),TRUE);

    $member_id = $post_json["member_id"];
    $race_id = $post_json["race_id"];
    $member_key = $post_json["member_key"];

    include '../../includes/dbConn.php';

    // attempt data base connection w/exception handling
    try {
        $db = new PDO($dsn, $username, $password, $options);


        //$sql = $db->prepare("SELECT member_race.race_id FROM member_race INNER JOIN memberLogin ON member_race.member_id = memberLogin.memberID WHERE member_race.race_id = 1 AND memberLogin.memberKey = :api_key");
        $sql = $db->prepare("SELECT member_race.race_id FROM member_race INNER JOIN memberLogin ON member_race.member_id = memberLogin.memberID WHERE member_race.race_id = 1 AND memberLogin.memberKey = :api_key");
        $sql->bindValue(":api_key", $member_key);
        $sql->execute();

        $result = $sql->fetch();

        $returnedresult = ($result!=null);

        if (!$returnedresult){

            echo "Bad API Key";

        } else {

            $sql = $db->prepare("INSERT INTO member_race (member_id,race_id,role_id) VALUES (:member_id, :race_id, 3)");
            $sql->bindValue(":member_id", $member_id);
            $sql->bindValue(":race_id", $race_id);

            $sql->execute();

            echo "Good API Key";
        }

        $result = null;
        $db = null;

    }catch (PDOException $e){

        $error = $e->getMessage();
        echo json_encode($error);

    }

}

function delete_runner(){

    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),TRUE);

    $member_id = $post_json["member_id"];
    $race_id = $post_json["race_id"];
    $member_key = $post_json["member_key"];

    include '../../includes/dbConn.php';

    // attempt data base connection w/exception handling
    try {
        $db = new PDO($dsn, $username, $password, $options);

        // verify valid api key for operator account
        $sql = $db->prepare("SELECT member_race.race_id FROM member_race INNER JOIN memberLogin ON member_race.member_id = memberLogin.memberID WHERE member_race.race_id = 1 AND memberLogin.memberKey = :api_key");
        $sql->bindValue(":api_key", $member_key);
        $sql->execute();

        $result = $sql->fetch();

        $returnedresult = ($result!=null);

        if (!$returnedresult){

            // return text if api key is invalid
            echo "Bad API Key";

        } else {

            // delete runner based on parameters passed via api
            $sql = $db->prepare("DELETE FROM member_race WHERE member_id = :member_id AND race_id = :race_id AND role_id = 3");
            $sql->bindValue(":member_id", $member_id);
            $sql->bindValue(":race_id", $race_id);

            $sql->execute();

        }

        $result = null;
        $db = null;

    }catch (PDOException $e){

        $error = $e->getMessage();
        echo json_encode($error);

    }

}

?>