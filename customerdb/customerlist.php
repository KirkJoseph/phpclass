<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <h3>Customer Database</h3>
    <table border="1" width="100%">
        <tr>
            <th>Customer ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>E-Mail</th>
            <th>Password</th>
        </tr>
        <?php
            // database connection/credentials
            include '../includes/dbConn.php';

            // attempt data base connection w/exception handling
            try {
                $db = new PDO($dsn, $username, $password, $options);

                // pull everything from table customerdb
                $sql = $db->prepare('select * from customerdb');

                $sql->execute();
                $row = $sql->fetch();

                // while we are getting data from table, display accordingly
                while ($row!=null){

                    echo "<tr>";
                    echo "<td><a href=customerupdate.php?id=" .$row["customerid"]. ">".$row["customerid"]."</a></td>";
                    echo "<td><a href=customerupdate.php?id=" .$row["customerid"]. ">".$row["firstname"]."</a></td>";
                    echo "<td><a href=customerupdate.php?id=" .$row["customerid"]. ">".$row["lastname"]."</a></td>";
                    echo "<td>".$row["address"]."</td>";
                    echo "<td>".$row["city"]."</td>";
                    echo "<td>".$row["state"]."</td>";
                    echo "<td>".$row["zipcode"]."</td>";
                    echo "<td>".$row["phone"]."</td>";
                    echo "<td>".$row["email"]."</td>";
                    echo "<td>".$row["password"]."</td>";
                    echo "</tr>";

                    $row = $sql->fetch();

                }

                // display error message if there was an exception
            }catch (PDOException $e){

                $error = $e->getMessage();
                echo "Error: $error";

            }

        ?>

    </table>
    <br>
    <br>
    <a href="customeradd.php">Add New Customer</a>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
