<?php

if(isset($_GET["id"]))
{

    $id = $_GET["id"];
    try {

        include '../includes/dbConn.php';

        $db = new PDO($dsn, $username, $password, $options);

        // select movie record based on ID passed from movielist

        $sql = $db->prepare("delete from customerdb where customerid = :id");

        $sql->bindValue(":id", $id);
        //$sql->bindValue(":Rating", $rating);

        $sql->execute();

        // display error message if there was an exception
    } catch (PDOException $e) {

        $error = $e->getMessage();
        echo "Error: $error";

    }

}

header("Location:customerlist.php");

?>