<?php
// see if variables are set due to form submission
$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
$errmsg = "";

if(isset($_POST["submit"])){

    if(isset($_POST["txtLastName"], $_POST["txtAddress"], $_POST["txtCity"], $_POST["txtState"], $_POST["txtZipcode"], $_POST["txtPhone"], $_POST["txtEmail"], $_POST["txtPassword"], $_POST["txtPassword2"]))
    {

        $custfn = $_POST["txtFirstName"];
        $custln = $_POST["txtLastName"];
        $custaddress = $_POST["txtAddress"];
        $custcity = $_POST["txtCity"];
        $custstate = $_POST["txtState"];
        $custzipcode = $_POST["txtZipcode"];
        $custphone = $_POST["txtPhone"];
        $custemail = $_POST["txtEmail"];
        $custpw = $_POST["txtPassword"];

        include '../includes/dbConn.php';

        try {
            $db = new PDO($dsn, $username, $password, $options);

            // insert form submission into table
            $sql = $db->prepare('insert into customerdb (firstname, lastname, address, city, state, zipcode, phone, email, password) value (:firstname, :lastname, :address, :city, :state, :zipcode, :phone, :email, :password)');

            $sql->bindValue(":firstname", $custfn);
            $sql->bindValue(":lastname", $custln);
            $sql->bindValue(":address", $custaddress);
            $sql->bindValue(":city", $custcity);
            $sql->bindValue(":state", $custstate);
            $sql->bindValue(":zipcode", $custzipcode);
            $sql->bindValue(":phone", $custphone);
            $sql->bindValue(":email", $custemail);
            $sql->bindValue(":password", md5($custpw.$key));

            $sql->execute();

            // display error message if there was an exception
        }catch (PDOException $e){

            $error = $e->getMessage();
            echo "Error: $error";

        }

        header("Location:customerlist.php");
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Joe's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <h3>Create Account</h3>
    <form method="post">
        <table border="0" width="80%">
            <tr>
                <td>
                    <fieldset>
                        <legend align="left">Customer</legend>
                        <label>First Name</label><input id="txtFirstName" name="txtFirstName" type="text" size="50" required><br>
                        <label>Last Name</label><input id="txtLastName" name="txtLastName" type="text" size="50" required><br>
                        <label>Phone #</label><input id="txtPhone" name="txtPhone" type="tel" size="50" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" placeholder="###-###-####"><br>
                        <label>E-Mail</label><input id="txtEmail" name="txtEmail" type="text" size="50" required>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend align="left">Address</legend>
                        <label>Address</label><input id="txtAddress" name="txtAddress" type="text" size="50"><br>
                        <label>City</label><input id="txtCity" name="txtCity" type="text" size="50" required><br>
                        <label>Zip Code</label><input id="txtZipcode" name="txtZipcode" type="text" size="50" pattern="[0-9]{5}" placeholder="#####" required><br>
                        <label>State</label><select name="txtState" id="txtState" required style="width: 52ch;">
                            <option value="" selected="selected">Select a State</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend align="left">Security</legend>
                        <label>Password</label><input type="password" name="txtPassword" id="txtPassword" size="50" required><br>
                        <label>Repeat Password</label><input type="password" name="txtPassword2" id="txtPassword2" size="50" required>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Create Account" name="submit"><input type="reset" value="Reset"></td>
            </tr>
        </table>
    </form>
    <br>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
